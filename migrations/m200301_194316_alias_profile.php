<?php

use yii\db\Migration;

/**
 * Class m200301_194316_alias_profile
 */
class m200301_194316_alias_profile extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%profile}}', 'alias', $this->string());
        $this->dropColumn('{{%profile}}', 'full_name');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200301_194316_alias_profile cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200301_194316_alias_profile cannot be reverted.\n";

        return false;
    }
    */
}
