<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `{{%lesson_dependency}}`.
 */
class m200229_074243_drop_lesson_dependency_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('{{%lesson_dependency}}');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createTable('{{%lesson_dependency}}', [
            'id' => $this->primaryKey(),
        ]);
    }
}
