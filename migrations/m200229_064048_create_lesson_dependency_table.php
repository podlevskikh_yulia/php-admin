<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%lesson_dependency}}`.
 */
class m200229_064048_create_lesson_dependency_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%lesson_dependency}}', [
            'id' => $this->primaryKey(),
            'lesson_id' => $this->integer()->notNull(),
            'dependent_lesson_id' => $this->integer()->notNull()
        ]);

        $this->createIndex(
            'idx-dependency-dependent_lesson_id',
            'lesson_dependency',
            'dependent_lesson_id'
        );

        $this->addForeignKey(
            'fk-lesson-dependency-dependent_lesson_id',
            'lesson_dependency',
            'dependent_lesson_id',
            'lesson',
            'id'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%lesson_dependency}}');
    }
}
