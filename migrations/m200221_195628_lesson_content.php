<?php

use yii\db\Schema;
use yii\db\Migration;

class m200221_195628_lesson_content extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%lesson_content}}', [
            'id' => $this->primaryKey(11),
            'lesson_id' => $this->integer(11)->notNull(),
            'content' => $this->text()->notNull(),
            'sort_weight' => $this->integer(11)->notNull()->defaultValue(0),
        ], $tableOptions);

        $this->createIndex('lesson_content_lesson_id_fk', '{{%lesson_content}}', ['lesson_id'], false);
        $this->addForeignKey(
            'fk_lesson_content_lesson_id',
            '{{%lesson_content}}', 'lesson_id',
            '{{%lesson}}', 'id',
            'CASCADE', 'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_lesson_content_lesson_id', '{{%lesson_content}}');
        $this->dropTable('{{%lesson_content}}');
    }
}
