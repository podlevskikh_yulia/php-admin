<?php

use yii\db\Schema;
use yii\db\Migration;

class m200221_195800_exercise extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%exercise}}', [
            'id' => $this->primaryKey(11),
            'is_active' => $this->tinyInteger(1)->notNull()->defaultValue(1),
            'is_final' => $this->tinyInteger(1)->notNull()->defaultValue(0)->comment('является ли упражнение частью итоговой работы'),
            'created_at' => $this->datetime()->notNull()->defaultExpression("CURRENT_TIMESTAMP"),
            'alias' => $this->string(255)->notNull(),
            'label' => $this->string(255)->notNull(),
            'description' => $this->text()->notNull()->comment('текст задания, описание (html)'),
            'type_id' => $this->integer(11)->notNull()->defaultValue(1)->comment('тип задания'),
            'lesson_id' => $this->integer(11)->notNull()->comment('занятие'),
            'difficulty_level' => $this->smallInteger(6)->notNull()->defaultValue(1)->comment('уровень сложности, 1 - просто, далее в сторону увеличения'),
            'sort' => $this->integer(11)->notNull()->defaultValue(1)->comment('сортировка'),
        ], $tableOptions);

        $this->createIndex('exercise_alias_uindex', '{{%exercise}}', ['alias'], true);
        $this->createIndex('exercise_lesson_id_fk', '{{%exercise}}', ['lesson_id'], false);
        $this->createIndex('exercise_exercise_type_id_fk', '{{%exercise}}', ['type_id'], false);
        $this->addForeignKey(
            'fk_exercise_type_id',
            '{{%exercise}}', 'type_id',
            '{{%exercise_type}}', 'id',
            'CASCADE', 'CASCADE'
        );
        $this->addForeignKey(
            'fk_exercise_lesson_id',
            '{{%exercise}}', 'lesson_id',
            '{{%lesson}}', 'id',
            'CASCADE', 'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_exercise_type_id', '{{%exercise}}');
        $this->dropForeignKey('fk_exercise_lesson_id', '{{%exercise}}');
        $this->dropTable('{{%exercise}}');
    }
}
