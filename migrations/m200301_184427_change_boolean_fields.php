<?php

use yii\db\Migration;

/**
 * Class m200301_184427_change_boolean_fields
 */
class m200301_184427_change_boolean_fields extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%course}}', 'is_active');
        $this->dropColumn('{{%exercise}}', 'is_active');
        $this->dropColumn('{{%exercise}}', 'is_final');
        $this->dropColumn('{{%exercise_answer}}', 'is_active');
        $this->dropColumn('{{%exercise_answer}}', 'is_true');
        $this->dropColumn('{{%lesson}}', 'is_active');
        $this->dropColumn('{{%section}}', 'is_active');

        $this->addColumn('{{%course}}', 'is_active', $this->boolean()->defaultValue(true));
        $this->addColumn('{{%exercise}}', 'is_active', $this->boolean()->defaultValue(true));
        $this->addColumn('{{%exercise}}', 'is_final', $this->boolean()->defaultValue(false));
        $this->addColumn('{{%exercise_answer}}', 'is_active', $this->boolean()->defaultValue(true));
        $this->addColumn('{{%exercise_answer}}', 'is_true', $this->boolean()->defaultValue(false));
        $this->addColumn('{{%lesson}}', 'is_active', $this->boolean()->defaultValue(true));
        $this->addColumn('{{%section}}', 'is_active', $this->boolean()->defaultValue(true));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200301_184427_change_boolean_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200301_184427_change_boolean_fields cannot be reverted.\n";

        return false;
    }
    */
}
