<?php

use yii\db\Schema;
use yii\db\Migration;

class m200221_195705_exercise_type extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%exercise_type}}', [
            'id' => $this->primaryKey(11),
            'alias' => $this->string(255)->notNull(),
            'label' => $this->string(255)->notNull(),
        ], $tableOptions);

        $this->createIndex('exercise_type_alias_uindex', '{{%exercise_type}}', ['alias'], true);
    }

    public function safeDown()
    {
        $this->dropTable('{{%exercise_type}}');
    }
}
