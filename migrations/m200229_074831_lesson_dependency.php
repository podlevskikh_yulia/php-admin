<?php

use yii\db\Schema;
use yii\db\Migration;

class m200229_074831_lesson_dependency extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%lesson_dependency}}', [
            'lesson_id' => $this->integer(32)->notNull(),
            'dependent_lesson_id' => $this->integer(32)->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('pk_on_lesson_dependency', '{{%lesson_dependency}}', ['lesson_id', 'dependent_lesson_id']);
        $this->addForeignKey(
            'fk_lesson_dependency_lesson_id',
            '{{%lesson_dependency}}', 'lesson_id',
            '{{%lesson}}', 'id'
        );
        $this->addForeignKey(
            'fk_lesson_dependency_dependent_lesson_id',
            '{{%lesson_dependency}}', 'dependent_lesson_id',
            '{{%lesson}}', 'id'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_lesson_dependency_lesson_id', '{{%lesson_dependency}}');
        $this->dropForeignKey('fk_lesson_dependency_dependent_lesson_id', '{{%lesson_dependency}}');
        $this->dropPrimaryKey('pk_on_lesson_dependency', '{{%lesson_dependency}}');
        $this->dropTable('{{%lesson_dependency}}');
    }
}
