<?php

use yii\db\Schema;
use yii\db\Migration;

class m200221_195340_section extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%section}}',
            [
                'id' => $this->primaryKey(11),
                'is_active' => $this->tinyInteger(1)->notNull()->defaultValue(1),
                'alias' => $this->string(255)->notNull(),
                'label' => $this->string(255)->notNull(),
                'description' => $this->text()->null()->defaultValue(null),
                'course_id' => $this->integer(11)->notNull()->comment('id курса к которому принадлежит раздел'),
            ], $tableOptions
        );
        $this->createIndex('section_alias_uindex', '{{%section}}', ['alias'], true);
        $this->createIndex('section_course_id_fk', '{{%section}}', ['course_id'], false);
        $this->addForeignKey('fk_section_course_id',
            '{{%section}}', 'course_id',
            '{{%course}}', 'id',
            'CASCADE', 'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropIndex('section_alias_uindex', '{{%section}}');
        $this->dropIndex('section_course_id_fk', '{{%section}}');
        $this->dropForeignKey('fk_section_course_id', '{{%section}}');
        $this->dropTable('{{%section}}');
    }
}
