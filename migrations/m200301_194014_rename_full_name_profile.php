<?php

use yii\db\Migration;

/**
 * Class m200301_194014_rename_full_name_profile
 */
class m200301_194014_rename_full_name_profile extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%profile}}', 'nickname', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200301_194014_rename_full_name_profile cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200301_194014_rename_full_name_profile cannot be reverted.\n";

        return false;
    }
    */
}
