<?php

use yii\db\Migration;

/**
 * Class m200222_070208_seed_types
 */
class m200222_070208_seed_types extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // insert types data
        $columns = ['alias', 'label'];
        $this->batchInsert('{{%lesson_type}}', $columns, [
            ["prostoe1", "Простое"],
        ]);
        $columns = ['alias', 'label'];
        $this->batchInsert('{{%exercise_type}}', $columns, [
            ["prostoe1", "С одним вариантом ответа"],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200222_070208_seed_types cannot be reverted.\n";

        return false;
    }

}
