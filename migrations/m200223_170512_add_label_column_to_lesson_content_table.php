<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%lesson_content}}`.
 */
class m200223_170512_add_label_column_to_lesson_content_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%lesson_content}}', 'label', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%lesson_content}}', 'label');
    }
}
