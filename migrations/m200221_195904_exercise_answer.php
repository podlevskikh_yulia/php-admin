<?php

use yii\db\Schema;
use yii\db\Migration;

class m200221_195904_exercise_answer extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%exercise_answer}}', [
            'id' => $this->primaryKey(11),
            'is_active' => $this->tinyInteger(1)->notNull()->defaultValue(1),
            'is_true' => $this->tinyInteger(1)->notNull()->defaultValue(1)->comment('верный ответ, на случай если у задания может быть несколько вариантов ответа'),
            'exercise_id' => $this->integer(11)->notNull(),
            'answer' => $this->text()->notNull()->comment('вариант ответа'),
        ], $tableOptions);

        $this->createIndex('exercise_answer_exercise_id_fk', '{{%exercise_answer}}', ['exercise_id'], false);
        $this->addForeignKey(
            'fk_exercise_answer_exercise_id',
            '{{%exercise_answer}}', 'exercise_id',
            '{{%exercise}}', 'id',
            'CASCADE', 'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_exercise_answer_exercise_id', '{{%exercise_answer}}');
        $this->dropTable('{{%exercise_answer}}');
    }
}
