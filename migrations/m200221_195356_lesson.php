<?php

use yii\db\Schema;
use yii\db\Migration;

class m200221_195356_lesson extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%lesson}}', [
            'id' => $this->primaryKey(11),
            'is_active' => $this->tinyInteger(1)->notNull()->defaultValue(1),
            'created_at' => $this->datetime()->notNull()->defaultExpression("CURRENT_TIMESTAMP"),
            'alias' => $this->string(255)->notNull(),
            'label' => $this->string(255)->notNull(),
            'description' => $this->text()->null()->defaultValue(null),
            'section_id' => $this->integer(11)->notNull()->comment('какому разделу принадлежит занятие'),
            'type_id' => $this->integer(11)->notNull()->defaultValue(1)->comment('тип занятия'),
            'difficulty_level' => $this->tinyInteger(4)->notNull()->defaultValue(1)->comment('сложность занятия'),
        ], $tableOptions);

        $this->createIndex('lesson_alias_uindex', '{{%lesson}}', ['alias'], true);
        $this->createIndex('lesson_section_id_fk', '{{%lesson}}', ['section_id'], false);
        $this->createIndex('lesson_lesson_type_id_fk', '{{%lesson}}', ['type_id'], false);
        $this->addForeignKey(
            'fk_lesson_type_id',
            '{{%lesson}}', 'type_id',
            '{{%lesson_type}}', 'id',
            'CASCADE', 'CASCADE'
        );
        $this->addForeignKey(
            'fk_lesson_section_id',
            '{{%lesson}}', 'section_id',
            '{{%section}}', 'id',
            'CASCADE', 'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_lesson_type_id', '{{%lesson}}');
        $this->dropForeignKey('fk_lesson_section_id', '{{%lesson}}');
        $this->dropTable('{{%lesson}}');
    }
}
