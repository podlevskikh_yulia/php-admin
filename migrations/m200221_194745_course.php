<?php

use yii\db\Schema;
use yii\db\Migration;

class m200221_194745_course extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%course}}', [
            'id' => $this->primaryKey(11),
            'owner_id' => $this->integer(11)->notNull()->comment('user_id пользователя, создавшего курс'),
            'is_active' => $this->tinyInteger(1)->notNull()->defaultValue(1),
            'alias' => $this->string(255)->notNull(),
            'label' => $this->string(255)->notNull(),
            'description' => $this->text()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('course_alias_uindex', '{{%course}}', ['alias'], true);
        $this->createIndex('course_user_id_fk', '{{%course}}', ['owner_id'], false);
        $this->addForeignKey(
            'fk_course_owner_id',
            '{{%course}}', 'owner_id',
            '{{%user}}', 'id',
            'CASCADE', 'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_course_owner_id', '{{%course}}');
        $this->dropTable('{{%course}}');
    }
}
