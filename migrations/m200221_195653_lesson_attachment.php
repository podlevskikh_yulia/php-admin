<?php

use yii\db\Schema;
use yii\db\Migration;

class m200221_195653_lesson_attachment extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%lesson_attachment}}', [
            'id' => $this->primaryKey(11),
            'lesson_id' => $this->integer(11)->notNull()->comment('ссылка на занятие'),
            'url' => $this->string(255)->notNull()->comment('url вложения'),
            'type' => $this->string(15)->notNull()->defaultValue('video')->comment('тип вложения'),
        ], $tableOptions);

        $this->createIndex('lesson_attachment_lesson_id_fk', '{{%lesson_attachment}}', ['lesson_id'], false);
        $this->addForeignKey(
            'fk_lesson_attachment_lesson_id',
            '{{%lesson_attachment}}', 'lesson_id',
            '{{%lesson}}', 'id',
            'CASCADE', 'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_lesson_attachment_lesson_id', '{{%lesson_attachment}}');
        $this->dropTable('{{%lesson_attachment}}');
    }
}
