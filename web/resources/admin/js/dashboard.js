(function ($) {
    'use strict';
    $(function () {

        /**
         * On studentsGroup select change
         */
        $('[name="students_group_id"]').on('select2:select', function() {
            var value = this.value;
            // get filtered subject options
            $.get('/admin/students-group-subject/subject-dropdown-list', {students_group_id: value})
                .done(function(data) {
                    reinitSelect2('subject_id', data);
                });
        });

        /**
         * On subject select change
         */
        $('[name="subject_id"]').on('select2:select', function() {
            var value = this.value;
            // get filtered studentsGroup options
            $.get('/admin/students-group-subject/students-group-dropdown-list', {subject_id: value})
                .done(function(data) {
                    reinitSelect2('students_group_id', data);
                });
        });

        /**
         * Check selection
         */
        $('[name="students_group_id"], [name="subject_id"]').on('select2:change', function() {
            var studentsGroupId = $('[name="students_group_id"]').val(),
                subjectId = $('[name="subject_id"]').val();

            if (studentsGroupId && subjectId) {

            }
        });

        /**
         * Reinit select2 with new options
         * @param name input name
         * @param data new options
         */
        function reinitSelect2(name, data) {
            var $select = $('[name="'+name+'"]'),
                select2Instance = $select.data('select2'),
                resetOptions = select2Instance.options.options;

            // clear all options
            $select .html('');
            // add new options
            $.each(data, function(id, label) {
                var newOption = new Option(label, id, false, false);
                $select.append(newOption);
            });

            // reinit
            $select
                .select2('destroy')
                .select2(resetOptions);
        }

    });
})(jQuery);