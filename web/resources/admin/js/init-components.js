(function () {
    /*Summernote editor*/
    if ($(".summernote").length) {
        $('.summernote').summernote({
            height: 300,
            tabsize: 4,
            lang: 'ru-RU',
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['style', 'bold', 'clear']],
                ['font', ['superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['height', ['height']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview']],
            ],
            callbacks: {
                onImageUpload: function (files) {
                    for (let i = 0; i < files.length; i++) {
                        $.upload(files[i]);
                    }
                }
            },
        });

        $.upload = function (file) {
            let out = new FormData();
            out.append('file', file, file.name);

            $.ajax({
                method: 'POST',
                url: Config.UPLOADER_URL,
                crossDomain: true,
                contentType: false,
                cache: false,
                processData: false,
                data: out,
                success: function (img) {
                    $('.summernote').summernote('insertImage', img.img_path);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.error(textStatus + " " + errorThrown);
                }
            });
        };
    }
})();