<?php

namespace app\modules\user\models;

use app\modules\user\Module;
use Lcobucci\JWT\Token;
use ReflectionException;
use Yii;
use yii\base\Exception;
use yii\base\InvalidArgumentException;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\swiftmailer\Mailer;
use yii\swiftmailer\Message;
use yii\helpers\Inflector;
use ReflectionClass;

/**
 * This is the model class for table "tbl_user".
 *
 * @property string $id
 * @property string $role_id
 * @property integer $status
 * @property string $email
 * @property string $username
 * @property string $password
 * @property string $auth_key
 * @property string $access_token
 * @property string $logged_in_ip
 * @property string $logged_in_at
 * @property string $created_ip
 * @property string $created_at
 * @property string $updated_at
 * @property string $banned_at
 * @property string $banned_reason
 *
 * @property Profile $profile
 * @property Role $role
 * @property UserToken[] $userTokens
 * @property UserAuth[] $userAuths
 */
class User extends ActiveRecord implements IdentityInterface
{
    /**
     * @var int Inactive status
     */
    const STATUS_INACTIVE = 0;

    /**
     * @var int Active status
     */
    const STATUS_ACTIVE = 1;

    /**
     * @var int Unconfirmed email status
     */
    const STATUS_UNCONFIRMED_EMAIL = 2;

    /**
     * @var string Temporary email domain
     */
    const EMAIL_DOMAIN = "@email.tmp";

    /**
     * @var string Current password - for account page updates
     */
    public $currentPassword;

    /**
     * @var string New password - for registration and changing password
     */
    public $newPassword;

    /**
     * @var string New password confirmation - for reset
     */
    public $newPasswordConfirm;

    /**
     * @var array Permission cache array
     */
    protected $permissionCache = [];

    /**
     * @var Module
     */
    public $module;

    /**
     * @inheritdoc
     */
    public function init()
    {
        if (!$this->module) {
            $this->module = Yii::$app->getModule("user");
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            // general email and username rules
            [['email', 'username'], 'string', 'max' => 255],
            [['email', 'username'], 'unique'],
            [['email', 'username'], 'filter', 'filter' => 'trim'],
            [['email'], 'email'],
            [['username'], 'match', 'pattern' => '/^\w+$/u', 'except' => 'social', 'message' => Yii::t('user', '{attribute} can contain only letters, numbers, and "_"')],

            // password rules
            [['newPassword'], 'string', 'min' => 3],
            [['newPassword'], 'filter', 'filter' => 'trim'],
            [['newPassword'], 'required', 'on' => ['register', 'reset']],
            [['newPasswordConfirm'], 'required', 'on' => ['reset']],
            [['newPasswordConfirm'], 'compare', 'compareAttribute' => 'newPassword', 'message' => Yii::t('user', 'Passwords do not match')],

            // account page
            [['currentPassword'], 'validateCurrentPassword', 'on' => ['account']],

            // admin crud rules
            [['role_id', 'status'], 'required', 'on' => ['admin']],
            [['role_id', 'status'], 'integer', 'on' => ['admin']],
            [['banned_at'], 'integer', 'on' => ['admin']],
            [['banned_reason'], 'string', 'max' => 255, 'on' => 'admin'],
        ];

        // add required for currentPassword on account page
        // only if $this->password is set (might be null from a social login)
        if ($this->password) {
            $rules[] = [['currentPassword'], 'required', 'on' => ['account']];
        }

        // add required rules for email/username depending on module properties
        if ($this->module->requireEmail) {
            $rules[] = ["email", "required"];
        }
        if ($this->module->requireUsername) {
            $rules[] = ["username", "required"];
        }

        return $rules;
    }

    /**
     * Validate current password (account page)
     */
    public function validateCurrentPassword()
    {
        if (!$this->validatePassword($this->currentPassword)) {
            $this->addError("currentPassword", "Текущий пароль введен неверно");
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'role_id' => 'ID роли',
            'status' => 'Статус',
            'email' => 'Email',
            'username' => 'Логин',
            'password' => 'Пароль',
            'auth_key' => 'Ключ доступа',
            'access_token' => 'Токен',
            'logged_in_ip' => 'IP последнего входа',
            'logged_in_at' => 'Последний вход',
            'created_ip' => 'IP',
            'created_at' => 'Создан',
            'updated_at' => 'Обновлен',
            'banned_at' => 'Забанен',
            'banned_reason' => 'Причина бана',

            // virtual attributes set above
            'currentPassword' => Yii::t('user', 'Current Password'),
            'newPassword' => $this->isNewRecord ? Yii::t('user', 'Password') : Yii::t('user', 'New Password'),
            'newPasswordConfirm' => Yii::t('user', 'New Password Confirm'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'value' => function () {
                    return gmdate("Y-m-d H:i:s");
                },
            ]
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getProfile()
    {
        $profile = $this->module->model("Profile");
        return $this->hasOne($profile::className(), ['user_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getRole()
    {
        $role = $this->module->model("Role");
        return $this->hasOne($role::className(), ['id' => 'role_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUserTokens()
    {
        $userToken = $this->module->model("UserToken");
        return $this->hasMany($userToken::className(), ['user_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUserAuths()
    {
        $userAuth = $this->module->model("UserAuth");
        return $this->hasMany($userAuth::className(), ['user_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    /*public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(["access_token" => $token]);
    }*/

    /**
     * {@inheritdoc}
     * @param Token $token
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['id' => $token->getClaim('uid')]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    /**
     * Validate password
     * @param string $password
     * @return bool
     */
    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->decryptByPassword(
                base64_decode($this->password),
                Yii::$app->params['master_pass']
            ) === $password;
//        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        // check if we're setting $this->password directly
        // handle it by setting $this->newPassword instead
        $dirtyAttributes = $this->getDirtyAttributes();
        if (isset($dirtyAttributes["password"])) {
            $this->newPassword = $dirtyAttributes["password"];
        }

        // encrypt new password if set
        if ($this->newPassword) {
            //$this->password = Yii::$app->security->generatePasswordHash($this->newPassword);
            $this->password = Yii::$app->security->encryptByPassword(
                $this->newPassword,
                Yii::$app->params['master_pass']
            );
            $this->password = base64_encode($this->password);
        }

        // convert banned_at checkbox to date
        if ($this->banned_at) {
            $this->banned_at = gmdate("Y-m-d H:i:s");
        }

        // ensure fields are null so they won't get set as empty string
        $nullAttributes = ["email", "username", "banned_at", "banned_reason"];
        foreach ($nullAttributes as $nullAttribute) {
            $this->$nullAttribute = $this->$nullAttribute ? $this->$nullAttribute : null;
        }

        return parent::beforeSave($insert);
    }

    /**
     * Set attributes for registration
     * @param int $roleId
     * @param string $status
     * @return static
     * @throws Exception
     */
    public function setRegisterAttributes($roleId, $status = null)
    {
        // set default attributes
        $attributes = [
            "role_id" => $roleId,
            "created_ip" => Yii::$app->request->userIP,
            "auth_key" => Yii::$app->security->generateRandomString(),
            "access_token" => Yii::$app->security->generateRandomString(),
            "status" => static::STATUS_ACTIVE,
        ];

        // determine if we need to change status based on module properties
        $emailConfirmation = $this->module->emailConfirmation;
        $requireEmail = $this->module->requireEmail;
        $useEmail = $this->module->useEmail;
        if ($status) {
            $attributes["status"] = $status;
        } elseif ($emailConfirmation && $requireEmail) {
            $attributes["status"] = static::STATUS_INACTIVE;
        } elseif ($emailConfirmation && $useEmail && $this->email) {
            $attributes["status"] = static::STATUS_UNCONFIRMED_EMAIL;
        }

        // set attributes and return
        $this->setAttributes($attributes, false);
        return $this;
    }

    /**
     * Check for email change
     * @return string|bool
     */
    public function checkEmailChange()
    {
        // check if user didn't change email
        if ($this->email == $this->getOldAttribute("email")) {
            return false;
        }

        // check if we need to confirm email change
        if (!$this->module->emailChangeConfirmation) {
            return false;
        }

        // check if user is removing email address (only valid if Module::$requireEmail = false)
        if (!$this->email) {
            return false;
        }

        // update status and email before returning new email
        $newEmail = $this->email;
        $this->status = static::STATUS_UNCONFIRMED_EMAIL;
        $this->email = $this->getOldAttribute("email");
        return $newEmail;
    }

    /**
     * Update login info (ip and time)
     * @return bool
     */
    public function updateLoginMeta()
    {
        $this->logged_in_ip = Yii::$app->request->userIP;
        $this->logged_in_at = gmdate("Y-m-d H:i:s");
        return $this->save(false, ["logged_in_ip", "logged_in_at"]);
    }

    /**
     * Confirm user email
     * @param string $newEmail
     * @return bool
     */
    public function confirm($newEmail)
    {
        // update status
        $this->status = static::STATUS_ACTIVE;

        // process $newEmail from a userToken
        //   check if another user already has that email
        $success = true;
        if ($newEmail) {
            $checkUser = static::findOne(["email" => $newEmail]);
            if ($checkUser) {
                $success = false;
            } else {
                $this->email = $newEmail;
            }
        }

        $this->save(false, ["email", "status"]);
        return $success;
    }

    /**
     * Check if user can do specified $permission
     * @param string $permissionName
     * @param array $params
     * @param bool $allowCaching
     * @return bool
     */
    public function can($permissionName, $params = [], $allowCaching = true)
    {
        // check for auth manager rbac
        // copied from \yii\web\User
        $auth = Yii::$app->getAuthManager();
        if ($auth) {
            if ($allowCaching && empty($params) && isset($this->permissionCache[$permissionName])) {
                return $this->permissionCache[$permissionName];
            }
            $access = $auth->checkAccess($this->getId(), $permissionName, $params);
            if ($allowCaching && empty($params)) {
                $this->permissionCache[$permissionName] = $access;
            }
            return $access;
        }

        // otherwise use our own custom permission (via the role table)
        return $this->role->checkPermission($permissionName);
    }

    /**
     * Get display name for the user
     * @return string|int
     */
    public function getDisplayName()
    {
        return $this->username ?: $this->email ?: $this->id;
    }

    /**
     * Send email confirmation to user
     * @param UserToken $userToken
     * @return int
     */
    public function sendEmailConfirmation($userToken)
    {
        /** @var Mailer $mailer */
        /** @var Message $message */

        // modify view path to module views
        $mailer = Yii::$app->mailer;
        $oldViewPath = $mailer->viewPath;
        $mailer->viewPath = $this->module->emailViewPath;

        // send email
        $user = $this;
        $profile = $user->profile;
        $email = $userToken->data ?: $user->email;
        $subject = Yii::$app->id . " - " . Yii::t("user", "Email Confirmation");
        $result = $mailer->compose('confirmEmail', compact("subject", "user", "profile", "userToken"))
            ->setTo($email)
            ->setSubject($subject)
            ->send();

        // restore view path and return result
        $mailer->viewPath = $oldViewPath;
        return $result;
    }

    /**
     * Get list of statuses for creating dropdowns
     * @return array
     * @throws ReflectionException
     */
    public static function statusDropdown()
    {
        // get data if needed
        static $dropdown;
        $constPrefix = "STATUS_";
        if ($dropdown === null) {

            // create a reflection class to get constants
            $reflClass = new ReflectionClass(get_called_class());
            $constants = $reflClass->getConstants();

            // check for status constants (e.g., STATUS_ACTIVE)
            foreach ($constants as $constantName => $constantValue) {

                // add prettified name to dropdown
                if (strpos($constantName, $constPrefix) === 0) {
                    $prettyName = str_replace($constPrefix, "", $constantName);
                    $prettyName = Inflector::humanize(strtolower($prettyName));
                    $dropdown[$constantValue] = $prettyName;
                }
            }
        }

        return $dropdown;
    }

    public static function getByRoleId($role_id)
    {
        return static::findOne(["role_id" => $role_id]);
    }

    public static function tableName()
    {
        return 'user';
    }

    /**
     * Generate random password
     * @param int $length
     * @return string
     */
    public static function generatePassword($length = 8)
    {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $count = mb_strlen($chars);

        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }

        return $result;
    }

    /**
     * Generate temporary email
     * @return string
     * @throws Exception
     */
    public static function generateTempEmail()
    {
        return Yii::$app->security->generateRandomString(16) . static::EMAIL_DOMAIN;
    }

    /**
     * Generate temporary name
     * @return string
     * @throws Exception
     */
    public static function generateTempUsername()
    {
        return Yii::$app->security->generateRandomString(16);
    }

    /**
     * Generate email / username
     * @return $this
     */
    public function generateAttrsFromProfile()
    {
        $profile = $this->profile;

        if (!empty($profile->alias)) {
            $this->email = $profile->alias . static::EMAIL_DOMAIN;
            $this->username = $profile->alias;
            return $this;
        } else {
            throw new InvalidArgumentException("Profile alias is null");
        }
    }
}