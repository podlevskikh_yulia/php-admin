<?php

namespace app\modules\user\models;

use app\models\exceptions\AlreadyExistsException;
use app\models\helpers\Inflector;
use app\modules\user\Module;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "tbl_profile".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $alias
 * @property string $nickname
 *
 * @property User $user
 */
class Profile extends ActiveRecord
{
    /**
     * @var Module
     */
    public $module;

    /**
     * @inheritdoc
     */
    public function init()
    {
        if (!$this->module) {
            $this->module = Yii::$app->getModule("user");
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nickname', 'alias'], 'string', 'max' => 255],
            [['nickname', 'alias'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('user', 'ID'),
            'user_id' => Yii::t('user', 'User ID'),
            'created_at' => Yii::t('user', 'Created At'),
            'updated_at' => Yii::t('user', 'Updated At'),
            'nickname' => Yii::t('user', 'Full Name'),
            'alias' => 'Алиас',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'value' => function () {
                    return gmdate("Y-m-d H:i:s");
                },
            ],
        ];
    }

    /**
     * Create full name and generate alias
     * @param bool|array $insert
     * @return bool
     * @throws AlreadyExistsException
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($this->isNewRecord) {
            $this->nickname = $this->generateNickname();
            $this->alias = Inflector::createAlias(
                $this->nickname,
                $this->tableName()
            );
        }

        return true;
    }

    /**
     * Check already exists
     * @return bool
     * @throws AlreadyExistsException
     */
    public function beforeValidate()
    {
//        if ($profile = Profile::find()
//            ->where(['full_name' => $this->generateFullName()])
//            ->one()) {
//            throw new AlreadyExistsException($profile);
//        }
        return parent::beforeValidate();
    }

    /**
     * Generate user full name
     * @return string
     */
    private function generateNickname()
    {
        $names = ['unicorn', 'fenix', 'comet', 'moose', 'goose'];
        $nickname = $names[mt_rand(0, sizeof($names) - 1)] . mt_rand(100000, 999999);
        return Inflector::createAlias($nickname, 'profile');
    }

    /**
     * @return ActiveQuery
     */
    public function getUser()
    {
        $user = $this->module->model("User");
        return $this->hasOne($user::className(), ['id' => 'user_id']);
    }

    /**
     * Set user id
     * @param int $userId
     * @return static
     */
    public function setUser($userId)
    {
        $this->user_id = $userId;
        return $this;
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'profile';
    }
}