<?php

use app\assets\AdminAsset;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var app\modules\user\models\forms\LoginForm $model
 */

AdminAsset::register($this);

$this->title = "Вход";
$this->params['breadcrumbs'][] = $this->title;
?>
<span class="seccon"><?= Yii::$app->request->getIsSecureConnection() ?></span>
<div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper auth-page">
        <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">
            <div class="row w-100">
                <div class="col-lg-4 mx-auto">
                    <div class="auto-form-wrapper mb-3">
                        <?php $form = ActiveForm::begin([
                            'id' => 'login-form',
                            'fieldConfig' => [
                                'labelOptions' => ['class' => 'label'],
                            ],

                        ]); ?>

                        <?= $form->field($model, 'email') ?>
                        <?= $form->field($model, 'password')->passwordInput() ?>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary submit-btn btn-block">Войти</button>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                    <p class="footer-text text-center">
                        <a target="_blank"><?= Yii::$app->params['adminEmail'] ?></a> © <?= date('Y') ?>
                    </p>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
</div>
