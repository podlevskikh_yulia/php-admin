<?php


namespace app\modules\api\controllers;


use sizeg\jwt\JwtHttpBearerAuth;
use Yii;
use yii\base\Controller;
use yii\filters\VerbFilter;
use yii\web\HttpException;

class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'authenticator' => [
                'class' => JwtHttpBearerAuth::class,
                'optional' => [
                    'login',
                ],
            ]
        ];
    }

    public function actionLogin()
    {
        // load post data and login
        $post = Yii::$app->request->post();
    }
}