<?php
/**
 * Created by PhpStorm.
 * User: a.vladislav
 * Date: 03.09.2018
 * Time: 22:34
 */

/**
 * @var $items array
 */

use app\modules\admin\widgets\Menu; ?>

<nav class="sidebar sidebar-offcanvas" id="sidebar">


    <?= Menu::widget([
        'items' => $items,
        'encodeLabels' => false,
        'options' => ['class' => 'nav'],
        'itemOptions' => ['class' => 'nav-item'],
        'linkTemplate' => '<a class="nav-link" href="{url}">
                                <i class="menu-icon mdi mdi-{ico}"></i>
                                <span class="menu-title">{label}</span>
                            </a>',
        'submenuTemplate' => '<div class="collapse">
                                    <ul class="nav flex-column sub-menu">
                                    {items}
                                    </ul>
                              </div>'
    ]) ?>

</nav>
