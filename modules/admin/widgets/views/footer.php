<?php
/**
 * Created by PhpStorm.
 * User: a.vladislav
 * Date: 03.09.2018
 * Time: 22:41
 */
?>

<footer class="footer">
    <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">
              <a href="mailto:<?= Yii::$app->params['adminEmail'] ?>" target="_blank"><?= Yii::$app->params['adminEmail'] ?></a> © <?= date('Y') ?></span>
        <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Made with
              <i class="mdi mdi-heart text-danger"></i> by V.A.
            </span>
    </div>
</footer>
