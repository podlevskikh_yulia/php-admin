<?php

namespace app\modules\admin\widgets;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

class Menu extends \yii\widgets\Menu
{
    /**
     * Renders the content of a menu item with icons.
     * Note that the container and the sub-menus are not rendered here.
     * @param array $item the menu item to be rendered. Please refer to [[items]] to see what data might be in the item.
     * @return string the rendering result
     */
    protected function renderItem($item)
    {
        if (isset($item['url'])) {
            $template = ArrayHelper::getValue($item, 'template', $this->linkTemplate);
            $template = $this->replaceIco($template, $item);

            return strtr($template, [
                '{url}' => Html::encode(Url::to($item['url'])),
                '{label}' => $item['label'],
            ]);
        }

        $template = ArrayHelper::getValue($item, 'template', $this->labelTemplate);
        $template = $this->replaceIco($template, $item);

        return strtr($template, [
            '{label}' => $item['label'],
        ]);
    }


    /**
     * @param $template string
     * @param $item array
     * @return string
     */
    protected function replaceIco($template, $item)
    {
        if (isset($item['ico'])) {
            return strtr($template, [
                '{ico}' => $item['ico'],
            ]);
        }

        return $template;
    }

}