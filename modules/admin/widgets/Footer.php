<?php
/**
 * Created by PhpStorm.
 * User: a.vladislav
 * Date: 03.09.2018
 * Time: 22:35
 */

namespace app\modules\admin\widgets;


use yii\base\Widget;

class Footer extends Widget
{
    public function run()
    {
        return $this->render('footer');
    }
}