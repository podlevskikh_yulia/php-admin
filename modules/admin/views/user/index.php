<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\user\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <?= Breadcrumbs::widget([
        'homeLink' => false,
        'links' => $this->params['breadcrumbs']
    ]) ?>


    <div class="card">
        <div class="card-body">
            <h2><?= Html::encode($this->title) ?></h2>
            <?php Pjax::begin(); ?>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <p>
                <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <div class="table-responsive">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        [
                            'attribute' => 'id',
                            'headerOptions' => ['width' => 100]
                        ],
                        [
                            'attribute' => 'role_id',
                            'headerOptions' => ['width' => 100]
                        ],
                        [
                            'attribute' => 'status',
                            'headerOptions' => ['width' => 100]
                        ],
                        'email:email',
                        'username:ntext',
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'header' => 'Действия',
                            'template' => '{update} {delete}',
                            'buttons' => [
                                'update' => function ($url) {
                                    return Html::a('<span class="mdi mdi-pencil"></span>', $url);
                                },
                                'delete' => function ($url, $model) {
                                    if ($model->role_id != 1)
                                        return Html::a('<span class="mdi mdi-delete"></span>', $url, [
                                            'data' => [
                                                'confirm' => 'Вы уверенны, что хотите удалить пользователя?',
                                                'method' => 'post',
                                            ]
                                        ]);
                                    else return null;
                                },
                            ],
                        ],
                    ],
                ]); ?>
            </div>
            <?php Pjax::end(); ?>
        </div>
    </div>

</div>
