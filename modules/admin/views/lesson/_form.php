<?php

use app\models\Lesson;
use app\models\LessonType;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Lesson */
/* @var $form yii\widgets\ActiveForm */

$types = LessonType::find()->all();
$types_list = ArrayHelper::map($types, 'id', 'label');

$lessons = Lesson::find()->all();
$lessons_list = ArrayHelper::map($lessons, 'id', 'label');
if (!$model->isNewRecord) {
    unset($lessons_list[$model->id]);
}

$dependencies = array_column($model->dependencies, 'dependent_lesson_id');
?>

<div class="lesson-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'label')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'difficulty_level')->dropDownList([
        1 => 'Поверхностное',
        2 => 'Стандартное',
        3 => 'Углубленное'
    ]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 10, 'class' => 'form-control summernote']) ?>

    <?= $form->field($model, 'type_id')->dropDownList($types_list, ['prompt' => 'Выберите тип']) ?>

    <?= $form->field($model, 'dependencies')->dropDownList($lessons_list, [
        'multiple' => 'multiple',
        'value' => $dependencies
    ])->label('Зависимость от других занятий') ?>

    <?= $form->field($model, 'is_active')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        <?php if ($model->isNewRecord): ?>
            <?= Html::submitButton('Сохранить и создать еще', ['class' => 'btn btn-info', 'name' => 'save-and-create']) ?>
        <?php endif ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
