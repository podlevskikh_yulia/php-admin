<?php

use app\models\Section;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\LessonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $section Section */

$this->title = "Занятия в разделе «{$section->label}»";
$this->params['breadcrumbs'][] = ['label' => 'Курсы', 'url' => ['/admin/course']];
$this->params['breadcrumbs'][] = ['label' => 'Разделы', 'url' => ["/admin/section?course_id={$section->course_id}"]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lesson-index">

    <?= Breadcrumbs::widget([
        'homeLink' => false,
        'links' => $this->params['breadcrumbs']
    ]) ?>

    <div class="card">
        <div class="card-body">
            <h2><?= Html::encode($this->title) ?></h2>
            <?php Pjax::begin(); ?>
            <p>
                <?= Html::a('Создать', ["create?section_id=$section->id"], ['class' => 'btn btn-success']) ?>
            </p>
            <div class="table-responsive">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        [
                            'attribute' => 'id',
                            'headerOptions' => ['width' => 100]
                        ],
                        [
                            'attribute' => 'is_active',
                            'format' => 'boolean',
                            'enableSorting' => false
                        ],
                        'label:ntext',
                        'difficulty_level',
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'header' => 'Действия',
                            'template' => '{update} {content} {exercises} {delete}',
                            'buttons' => [
                                'update' => function ($url) {
                                    return Html::a(
                                        '<span class="mdi mdi-pencil"></span>',
                                        $url,
                                        ['title' => 'Изменить']
                                    );
                                },
                                'content' => function ($url, $model) {
                                    return Html::a(
                                        '<span class="mdi mdi-clipboard-text"></span>',
                                        "/admin/content?lesson_id=$model->id",
                                        ['title' => 'Страницы', 'data-pjax' => '0']
                                    );
                                },
                                'exercises' => function ($url, $model) {
                                    return Html::a(
                                        '<span class="mdi mdi-star-circle"></span>',
                                        "/admin/exercise?lesson_id=$model->id",
                                        ['title' => 'Задания', 'data-pjax' => '0']
                                    );
                                },
                                'delete' => function ($url) {
                                    return Html::a('<span class="mdi mdi-delete"></span>', $url, [
                                        'title' => 'Удалить занятие',
                                        'data' => [
                                            'confirm' => 'Вы уверенны, что хотите удалить занятие?',
                                            'method' => 'post',
                                        ]
                                    ]);
                                },
                            ],
                        ],
                    ],
                ]); ?>
            </div>
            <?php Pjax::end(); ?>
        </div>
    </div>

</div>
