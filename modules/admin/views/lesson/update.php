<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model app\models\Lesson */

$this->title = 'Изменить занятие: ' . $model->label;
$this->params['breadcrumbs'][] = ['label' => 'Курсы', 'url' => ['/admin/course']];
$this->params['breadcrumbs'][] = ['label' => 'Разделы', 'url' => ["/admin/section?course_id={$model->section->course_id}"]];
$this->params['breadcrumbs'][] = ['label' => 'Занятия', 'url' => ["index?section_id={$model->section_id}"]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="lesson-update">

    <?= Breadcrumbs::widget([
        'homeLink' => false,
        'links' => $this->params['breadcrumbs']
    ]) ?>

    <div class="card">
        <div class="card-body">

            <h2><?= Html::encode($this->title) ?></h2>

            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>

        </div>
    </div>

</div>
