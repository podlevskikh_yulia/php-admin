<?php

use app\models\Section;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model app\models\Lesson */
/* @var $section Section */

$this->title = 'Создать занятие';
$this->params['breadcrumbs'][] = ['label' => 'Курсы', 'url' => ['/admin/course']];
$this->params['breadcrumbs'][] = ['label' => 'Разделы', 'url' => ["/admin/section?course_id={$section->course_id}"]];
$this->params['breadcrumbs'][] = ['label' => 'Занятия', 'url' => ["index?section_id={$section->id}"]];
$this->params['breadcrumbs'][] = 'Создать';
?>
<div class="lesson-create">

    <?= Breadcrumbs::widget([
        'homeLink' => false,
        'links' => $this->params['breadcrumbs']
    ]) ?>

    <div class="card">
        <div class="card-body">

            <h2>Новое занятие</h2>

            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>

        </div>
    </div>

</div>
