<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\LessonContentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $lesson app\models\Lesson */

$this->title = "Страницы в занятии «{$lesson->label}»";
$this->params['breadcrumbs'][] = ['label' => 'Курсы', 'url' => ['/admin/course']];
$this->params['breadcrumbs'][] = ['label' => 'Разделы', 'url' => ["/admin/section?course_id={$lesson->section->course_id}"]];
$this->params['breadcrumbs'][] = ['label' => 'Занятия', 'url' => ["/admin/lesson?section_id={$lesson->section_id}"]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lesson-content-index">

    <?= Breadcrumbs::widget([
        'homeLink' => false,
        'links' => $this->params['breadcrumbs']
    ]) ?>

    <div class="card">
        <div class="card-body">
            <h2><?= Html::encode($this->title) ?></h2>
            <?php Pjax::begin(); ?>
            <p>
                <?= Html::a('Создать', ["create?lesson_id=$lesson->id"], ['class' => 'btn btn-success']) ?>
            </p>
            <div class="table-responsive">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        [
                            'attribute' => 'id',
                            'headerOptions' => ['width' => 100]
                        ],
                        'sort_weight',
                        'label:ntext',
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'header' => 'Действия',
                            'template' => '{update} {delete}',
                            'buttons' => [
                                'update' => function ($url) {
                                    return Html::a(
                                        '<span class="mdi mdi-pencil"></span>',
                                        $url,
                                        ['title' => 'Изменить']
                                    );
                                },
                                'delete' => function ($url) {
                                    return Html::a('<span class="mdi mdi-delete"></span>', $url, [
                                        'title' => 'Удалить страницу',
                                        'data' => [
                                            'confirm' => 'Вы уверенны, что хотите удалить страницу?',
                                            'method' => 'post',
                                        ]
                                    ]);
                                },
                            ],
                        ],
                    ],
                ]); ?>
            </div>
            <?php Pjax::end(); ?>
        </div>
    </div>

</div>
