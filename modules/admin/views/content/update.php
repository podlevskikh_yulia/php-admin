<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model app\models\LessonContent */

$this->title = "Обновить страницу занятия «{$model->lesson->label}»";
$this->params['breadcrumbs'][] = ['label' => 'Курсы', 'url' => ['/admin/course']];
$this->params['breadcrumbs'][] = ['label' => 'Разделы', 'url' => ["/admin/section?course_id={$model->lesson->section->course_id}"]];
$this->params['breadcrumbs'][] = ['label' => 'Занятия', 'url' => ["/admin/lesson?section_id={$model->lesson->section_id}"]];
$this->params['breadcrumbs'][] = ['label' => 'Страницы', 'url' => ["index?lesson_id={$model->lesson_id}"]];
$this->params['breadcrumbs'][] = 'Обновить страницу';
?>
<div class="lesson-content-update">

    <?= Breadcrumbs::widget([
        'homeLink' => false,
        'links' => $this->params['breadcrumbs']
    ]) ?>

    <div class="card">
        <div class="card-body">

            <h2><?= Html::encode($this->title) ?></h2>

            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>

        </div>
    </div>

</div>
