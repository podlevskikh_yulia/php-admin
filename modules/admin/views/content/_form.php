<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LessonContent */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lesson-content-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'label')->textInput() ?>

    <?= $form->field($model, 'content')->textarea(['rows' => 20, 'class' => 'form-control summernote']) ?>

    <?= $form->field($model, 'sort_weight')->input('number') ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        <?php if ($model->isNewRecord): ?>
            <?= Html::submitButton('Сохранить и создать еще', ['class' => 'btn btn-info', 'name' => 'save-and-create']) ?>
        <?php endif ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
