<?php

use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model app\models\Section */
/* @var $course_id integer */

$this->title = 'Создать раздел';
$this->params['breadcrumbs'][] = ['label' => 'Курсы', 'url' => ['/admin/course']];
$this->params['breadcrumbs'][] = ['label' => 'Разделы', 'url' => ["index?course_id={$course_id}"]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="section-create">

    <?= Breadcrumbs::widget([
        'homeLink' => false,
        'links' => $this->params['breadcrumbs']
    ]) ?>

    <div class="card">
        <div class="card-body">

            <h2>Новый раздел</h2>

            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>

        </div>
    </div>

</div>
