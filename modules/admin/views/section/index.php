<?php

use app\models\Course;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\SectionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $course Course */

$this->title = "Разделы курса «{$course->label}»";
$this->params['breadcrumbs'][] = ['label' => 'Курсы', 'url' => ['/admin/course']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="section-index">

    <?= Breadcrumbs::widget([
        'homeLink' => false,
        'links' => $this->params['breadcrumbs']
    ]) ?>

    <div class="card">
        <div class="card-body">
            <h2><?= Html::encode($this->title) ?></h2>
            <?php Pjax::begin(); ?>
            <p>
                <?= Html::a('Создать', ["create?course_id=$course->id"], ['class' => 'btn btn-success']) ?>
            </p>
            <div class="table-responsive">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        [
                            'attribute' => 'id',
                            'headerOptions' => ['width' => 100]
                        ],
                        [
                            'attribute' => 'is_active',
                            'format' => 'boolean',
                            'enableSorting' => false
                        ],
                        'label:ntext',

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'header' => 'Действия',
                            'template' => '{lessons} {update}',
                            'buttons' => [
                                'lessons' => function ($url, $model) {
                                    return Html::a(
                                        '<span class="mdi mdi-clipboard-text"></span>',
                                        "/admin/lesson?section_id=$model->id",
                                        ['title' => 'Занятия', 'data-pjax' => '0']
                                    );
                                },
                                'update' => function ($url) {
                                    return Html::a(
                                        '<span class="mdi mdi-pencil"></span>',
                                        $url,
                                        ['title' => 'Изменить']
                                    );
                                },
                            ],
                        ],
                    ],
                ]); ?>
            </div>
            <?php Pjax::end(); ?>
        </div>
    </div>

</div>
