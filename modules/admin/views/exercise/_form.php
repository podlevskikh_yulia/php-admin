<?php

use app\assets\VueAsset;
use app\models\ExerciseAnswer;
use app\models\ExerciseType;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

VueAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Exercise */
/* @var $form yii\widgets\ActiveForm */

$types = ExerciseType::find()->all();
$types_list = ArrayHelper::map($types, 'id', 'label');

if (count($model->answers) > 0) {
    $answer = $model->answers[0];
} else {
    $answer = new ExerciseAnswer;
}

?>

<div class="exercise-form" id="exercise-form-vue">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'label')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type_id')->dropDownList($types_list, [
        'prompt' => 'Выберите тип',
        'v-model' => 'exercise_type'
    ]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 10, 'class' => 'form-control summernote']) ?>

    <div v-if="exercise_type == 1">
        <?= $form->field($answer, 'answer')->textInput() ?>
    </div>

    <?= $form->field($model, 'difficulty_level')->dropDownList([
        1 => 'Базовый',
        2 => 'Повышенный',
        3 => 'Высокий'
    ]) ?>

    <?= $form->field($model, 'sort')->input('number') ?>

    <?= $form->field($model, 'is_active')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        <?php if ($model->isNewRecord): ?>
            <?= Html::submitButton('Сохранить и создать еще', ['class' => 'btn btn-info', 'name' => 'save-and-create']) ?>
        <?php endif ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    const app = new Vue({
        el: '#exercise-form-vue',
        data: {
            exercise_type: 1
        }
    });
</script>
