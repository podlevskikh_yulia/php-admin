<?php

use app\models\Lesson;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model app\models\Exercise */
/* @var $lesson Lesson */

$this->title = 'Создать страницу';
$this->params['breadcrumbs'][] = ['label' => 'Курсы', 'url' => ['/admin/course']];
$this->params['breadcrumbs'][] = ['label' => 'Разделы', 'url' => ["/admin/section?course_id={$lesson->section->course_id}"]];
$this->params['breadcrumbs'][] = ['label' => 'Занятия', 'url' => ["/admin/lesson?section_id={$lesson->section_id}"]];
$this->params['breadcrumbs'][] = ['label' => 'Задания', 'url' => ["index?lesson_id={$lesson->id}"]];
$this->params['breadcrumbs'][] = 'Создать';
?>
<div class="exercise-create">

    <?= Breadcrumbs::widget([
        'homeLink' => false,
        'links' => $this->params['breadcrumbs']
    ]) ?>

    <div class="card">
        <div class="card-body">

            <h2>Новое задание</h2>

            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>

        </div>
    </div>

</div>
