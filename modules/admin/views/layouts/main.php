<?php
/**
 * Created by PhpStorm.
 * User: a.vladislav
 * Date: 03.09.2018
 * Time: 22:33
 */

/**
 * @var $content string
 */

use app\assets\AdminAsset;
use app\modules\admin\widgets\Footer;
use app\modules\admin\widgets\Nav;
use app\modules\admin\widgets\SideBar;
use app\widgets\FlashAlert;
use yii\helpers\Html;

AdminAsset::register($this);

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Административный раздел</title>
    <link rel="shortcut icon" href="/img/admin/favicon.png"/>
</head>

<script>
    Config = {
        UPLOADER_URL: "<?= env('UPLOADER_URL') ?>"
    }
</script>

<body>
<?php $this->beginBody() ?>
<div class="container-scroller">

    <?= Nav::widget() ?>

    <div class="container-fluid page-body-wrapper">

        <?= SideBar::widget([
            'items' => [
                ['label' => 'Пользователи', 'url' => ['/admin/user'], 'ico' => 'account-multiple'],
                ['label' => 'Курсы', 'url' => ['/admin/course'], 'ico' => 'briefcase'],
            ]
        ]) ?>

        <div class="main-panel">
            <div class="content-wrapper">

                <?= FlashAlert::widget() ?>

                <?= $content ?>
            </div>
            <!-- content-wrapper ends -->

                <?= Footer::widget() ?>

            </div>
            <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
<?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>

