<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Курсы';
?>
<div class="course-index">

    <div class="card">
        <div class="card-body">
            <h2><?= Html::encode($this->title) ?></h2>
            <?php Pjax::begin(); ?>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <p>
                <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <div class="table-responsive">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        [
                            'attribute' => 'id',
                            'headerOptions' => ['width' => 100]
                        ],
                        [
                            'attribute' => 'owner_id',
                            'value' => 'owner.username',
                            'headerOptions' => ['width' => 100]
                        ],
                        [
                            'attribute' => 'is_active',
                            'format' => 'boolean',
                            'enableSorting' => false
                        ],
                        'label:ntext',

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'header' => 'Действия',
                            'template' => '{sections} {update}',
                            'buttons' => [
                                'sections' => function ($url, $model) {
                                    return Html::a(
                                        '<span class="mdi mdi-folder"></span>',
                                        "/admin/section?course_id=$model->id",
                                        ['title' => 'Разделы', 'data-pjax' => '0']
                                    );
                                },
                                'update' => function ($url) {
                                    return Html::a(
                                        '<span class="mdi mdi-pencil"></span>',
                                        $url,
                                        ['title' => 'Изменить']
                                    );
                                },
                            ],
                        ],
                    ],
                ]); ?>
            </div>
            <?php Pjax::end(); ?>
        </div>
    </div>

</div>
