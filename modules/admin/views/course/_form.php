<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Course */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="course-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'label')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6, 'class' => 'form-control summernote']) ?>

    <?= $form->field($model, 'is_active')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        <?php if ($model->isNewRecord): ?>
            <?= Html::submitButton('Сохранить и создать еще', ['class' => 'btn btn-info', 'name' => 'save-and-create']) ?>
        <?php endif ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
