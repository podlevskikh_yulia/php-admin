<?php

namespace app\modules\admin\controllers;

use app\models\Lesson;
use Throwable;
use Yii;
use app\models\LessonContent;
use app\models\search\LessonContentSearch;
use yii\db\StaleObjectException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LessonContentController implements the CRUD actions for LessonContent model.
 */
class ContentController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LessonContent models.
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        $params = Yii::$app->request->queryParams;
        $lessonId = $params['lesson_id'];

        if (empty($lessonId)) {
            throw new NotFoundHttpException("Страница не найдена");
        }

        $lesson = Lesson::findOne($lessonId);

        $searchModel = new LessonContentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'lesson' => $lesson
        ]);
    }

    /**
     * Creates a new LessonContent model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionCreate()
    {
        $params = Yii::$app->request->queryParams;
        $lessonId = $params['lesson_id'];

        if (empty($lessonId)) {
            throw new NotFoundHttpException("Страница не найдена");
        }

        $lesson = Lesson::findOne($lessonId);
        $model = new LessonContent();

        if ($model->load(Yii::$app->request->post())) {
            $model->lesson_id = $lesson->id;
            if ($model->save()) {
                Yii::$app->session->setFlash('success', "Страница успешно создана!");
                if (!is_null(Yii::$app->request->post('save-and-create'))) {
                    return $this->redirect(["create?lesson_id=$lesson->id"]);
                }
                return $this->redirect(["index?lesson_id=$lesson->id"]);
            }
        }

        if (!$model->getErrors()) {
            $model->sort_weight = count($lesson->contents) + 1;
        }

        return $this->render('create', [
            'model' => $model,
            'lesson' => $lesson
        ]);
    }

    /**
     * Updates an existing LessonContent model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Страница успешно обновлена!");
            return $this->redirect(["index?lesson_id={$model->lesson->id}"]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing LessonContent model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete($id)
    {
        $content = $this->findModel($id);
        $lessonId = $content->lesson_id;
        $content->delete();
        Yii::$app->session->setFlash('info', "Страница удалена");
        return $this->redirect(["index?lesson_id=$lessonId"]);
    }

    /**
     * Finds the LessonContent model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LessonContent the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LessonContent::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
