<?php

namespace app\modules\admin\controllers;

use app\models\LessonDependency;
use app\models\Section;
use Throwable;
use Yii;
use app\models\Lesson;
use app\models\search\LessonSearch;
use yii\db\StaleObjectException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LessonController implements the CRUD actions for Lesson model.
 */
class LessonController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Lesson models.
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        $params = Yii::$app->request->queryParams;
        $sectionId = $params['section_id'];

        if (empty($sectionId)) {
            throw new NotFoundHttpException("Страница не найдена");
        }

        $section = Section::findOne($sectionId);

        $searchModel = new LessonSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'section' => $section
        ]);
    }

    /**
     * Creates a new Lesson model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionCreate()
    {
        $params = Yii::$app->request->queryParams;
        $sectionId = $params['section_id'];

        if (empty($sectionId)) {
            throw new NotFoundHttpException("Страница не найдена");
        }

        $section = Section::findOne($sectionId);
        $model = new Lesson();

        if ($model->load(Yii::$app->request->post())) {
            $model->section_id = $section->id;
            if ($model->save()) {
                $this->updateDependencies($model->id);
                Yii::$app->session->setFlash('success', "Занятие успешно создано!");
                if (!is_null(Yii::$app->request->post('save-and-create'))) {
                    return $this->redirect(["create?section_id=$section->id"]);
                }
                return $this->redirect(["index?section_id=$section->id"]);
            }
        }

        if (!$model->getErrors()) {
            $model->is_active = true;
            $model->type_id = 1;
        }

        return $this->render('create', [
            'model' => $model,
            'section' => $section
        ]);
    }

    /**
     * Updates an existing Lesson model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->updateDependencies($model->id);
            Yii::$app->session->setFlash('success', "Занятие успешно обновлено!");
            return $this->redirect(["index?section_id=$model->section_id"]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Lesson model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete($id)
    {
        $lesson = $this->findModel($id);
        $sectionId = $lesson->section_id;
        Yii::$app->session->setFlash('info', "Занятие «{$lesson->label}» удалено");
        $lesson->delete();
        return $this->redirect(["index?section_id=$sectionId"]);
    }

    /**
     * Finds the Lesson model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Lesson the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Lesson::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function updateDependencies($id)
    {
        $post = Yii::$app->request->post()['Lesson'];
        LessonDependency::deleteAll(['lesson_id' => $id]);
        if (isset($post['dependencies'])) {
            foreach ($post['dependencies'] as $dependency_id) {
                $dependency = new LessonDependency();
                $dependency->lesson_id = $id;
                $dependency->dependent_lesson_id = $dependency_id;
                $dependency->save();
            }
        }
    }
}
