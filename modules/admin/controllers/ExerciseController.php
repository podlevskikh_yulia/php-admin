<?php

namespace app\modules\admin\controllers;

use app\models\ExerciseAnswer;
use app\models\Lesson;
use Throwable;
use Yii;
use app\models\Exercise;
use app\models\search\ExerciseSearch;
use yii\db\StaleObjectException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ExerciseController implements the CRUD actions for Exercise model.
 */
class ExerciseController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Exercise models.
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        $params = Yii::$app->request->queryParams;
        $lessonId = $params['lesson_id'];

        if (empty($lessonId)) {
            throw new NotFoundHttpException("Страница не найдена");
        }

        $lesson = Lesson::findOne($lessonId);

        $searchModel = new ExerciseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'lesson' => $lesson
        ]);
    }

    /**
     * Creates a new Exercise model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws NotFoundHttpException
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function actionCreate()
    {
        $params = Yii::$app->request->queryParams;
        $lessonId = $params['lesson_id'];

        if (empty($lessonId)) {
            throw new NotFoundHttpException("Страница не найдена");
        }

        $lesson = Lesson::findOne($lessonId);
        $model = new Exercise();

        if ($model->load(Yii::$app->request->post())) {
            $model->lesson_id = $lesson->id;
            if ($model->save() && $model->attachAnswers(Yii::$app->request->post())) {
                Yii::$app->session->setFlash('success', "Задание успешно создано!");
                if (!is_null(Yii::$app->request->post('save-and-create'))) {
                    return $this->redirect(["create?lesson_id=$lesson->id"]);
                }
                return $this->redirect(["index?lesson_id=$lesson->id"]);
            }
        }

        if (!$model->getErrors()) {
            $model->is_active = true;
            $model->type_id = 1;
            $model->sort = 0;
        }

        return $this->render('create', [
            'model' => $model,
            'lesson' => $lesson
        ]);
    }

    /**
     * Updates an existing Exercise model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (
            $model->load(Yii::$app->request->post())
            && $model->save()
            && $model->attachAnswers(Yii::$app->request->post())
        ) {
            Yii::$app->session->setFlash('success', "Задание успешно обновлено!");
            return $this->redirect(["index?lesson_id=$model->lesson_id"]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Exercise model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete($id)
    {
        $exercise = $this->findModel($id);
        $lessonId = $exercise->section_id;
        Yii::$app->session->setFlash('info', "Задание «{$exercise->label}» удалено");
        $exercise->delete();
        return $this->redirect(["index?lesson_id=$lessonId"]);
    }

    /**
     * Finds the Exercise model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Exercise the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Exercise::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
