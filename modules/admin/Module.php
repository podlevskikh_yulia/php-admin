<?php

namespace app\modules\admin;

use Yii;

/**
 * admin module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\admin\controllers';

    public $layout = '@app/modules/admin/views/layouts/main.php';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->setAliases([
            'admin-resources' => '@app/modules/admin/resources'
        ]);

        if (Yii::$app->user->isGuest || Yii::$app->user->getIdentity()->role->id != 1) {
            Yii::$app->response->redirect(['/user/login']);
            Yii::$app->end();
        }
    }
}
