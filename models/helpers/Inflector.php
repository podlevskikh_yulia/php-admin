<?php
/**
 * Created by PhpStorm.
 * User: a.vladislav
 * Date: 07.09.2018
 * Time: 2:06
 */

namespace app\models\helpers;


use yii\db\Query;
use yii\helpers\BaseInflector;

class Inflector extends BaseInflector
{

    private static $rus_alphabet = array(
        'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й',
        'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф',
        'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я',
        'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й',
        'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф',
        'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я'
    );

    private static $rus_alphabet_translit = array(
        'A', 'B', 'V', 'G', 'D', 'E', 'IO', 'ZH', 'Z', 'I', 'I',
        'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F',
        'H', 'C', 'CH', 'SH', 'SH', '', 'Y', '', 'E', 'IU', 'IA',
        'a', 'b', 'v', 'g', 'd', 'e', 'io', 'zh', 'z', 'i', 'i',
        'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f',
        'h', 'c', 'ch', 'sh', 'sch', '', 'y', '', 'e', 'iu', 'ia'
    );

    private static $underscore_delimiters = array(
        ' ', '/', '\\', '@', '^', '$', "\n"
    );

    private static function generateAlias($text)
    {
        $text = str_replace(self::$rus_alphabet, self::$rus_alphabet_translit, $text);
        $text = str_replace(self::$underscore_delimiters, '_', $text);
        return strtolower(self::removeExtraCharacters($text));
    }

    private static function aliasExists($table, $alias, $id)
    {
        $result = (new Query)->select('count(1)')
            ->from($table)
            ->where('alias = :alias', [':alias' => $alias])
            ->andWhere('id != :id', [':id' => $id])
            ->scalar();

        if ($result) {
            return true;
        }

        return false;
    }

    private static function removeExtraCharacters($text)
    {
        return preg_replace('/[^\+\-\w]/', '', $text);
    }

    public static function createAlias($text, $table, $id = 0)
    {
        $alias = self::generateAlias($text);

        if (self::aliasExists($table, $alias, $id)) {
            $i = 0;

            while (self::aliasExists($table, $alias . $i, $id))
                $i++;

            $alias = $alias.$i;
        }

        return $alias;
    }

}