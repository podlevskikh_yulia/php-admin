<?php
/**
 * Created by PhpStorm.
 * User: a.vladislav
 * Date: 23.09.2018
 * Time: 19:15
 */

namespace app\models\exceptions;


use yii\base\Exception;
use yii\db\ActiveRecord;

/**
 * Class AlreadyExistsException
 * @package app\models\exceptions
 */
class AlreadyExistsException extends Exception
{

    /** @var  ActiveRecord */
    public $model;

    /**
     * AlreadyExistsException constructor.
     * @param ActiveRecord $model
     */
    public function __construct($model)
    {
        $this->model = $model;
        parent::__construct("Active record already exists", 0, null);
    }

}