<?php

namespace app\models;

use app\models\query\LessonDependencyQuery;
use app\models\query\LessonQuery;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "lesson_dependency".
 *
 * @property int $lesson_id
 * @property int $dependent_lesson_id
 *
 * @property Lesson $lesson
 * @property Lesson $dependentLesson
 */
class LessonDependency extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lesson_dependency';
    }

    public static function primaryKey()
    {
        return ['lesson_id', 'dependent_lesson_id'];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lesson_id', 'dependent_lesson_id'], 'required'],
            [['lesson_id', 'dependent_lesson_id'], 'default', 'value' => null],
            [['lesson_id', 'dependent_lesson_id'], 'integer'],
            [['lesson_id', 'dependent_lesson_id'], 'unique', 'targetAttribute' => ['lesson_id', 'dependent_lesson_id']],
            [['lesson_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lesson::className(), 'targetAttribute' => ['lesson_id' => 'id']],
            [['dependent_lesson_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lesson::className(), 'targetAttribute' => ['dependent_lesson_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'lesson_id' => 'Lesson ID',
            'dependent_lesson_id' => 'Dependent Lesson ID',
        ];
    }

    /**
     * @return ActiveQuery|LessonQuery
     */
    public function getLesson()
    {
        return $this->hasOne(Lesson::className(), ['id' => 'lesson_id']);
    }

    /**
     * @return ActiveQuery|LessonQuery
     */
    public function getDependentLesson()
    {
        return $this->hasOne(Lesson::className(), ['id' => 'dependent_lesson_id']);
    }

    /**
     * {@inheritdoc}
     * @return LessonDependencyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LessonDependencyQuery(get_called_class());
    }
}
