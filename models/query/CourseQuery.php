<?php

namespace app\models\query;

use app\models\Course;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\app\models\Course]].
 *
 * @see \app\models\Course
 */
class CourseQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Course[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Course|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
