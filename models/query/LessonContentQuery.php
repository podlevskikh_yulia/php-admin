<?php

namespace app\models\query;

use app\models\LessonContent;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\app\models\LessonContent]].
 *
 * @see \app\models\LessonContent
 */
class LessonContentQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return LessonContent[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return LessonContent|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
