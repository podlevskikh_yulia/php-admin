<?php

namespace app\models\query;

use app\models\Section;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\app\models\Section]].
 *
 * @see \app\models\Section
 */
class SectionQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Section[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Section|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
