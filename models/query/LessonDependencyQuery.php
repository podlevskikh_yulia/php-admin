<?php

namespace app\models\query;

use app\models\LessonDependency;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\app\models\LessonDependency]].
 *
 * @see \app\models\LessonDependency
 */
class LessonDependencyQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return LessonDependency[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return LessonDependency|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
