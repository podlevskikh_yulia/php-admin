<?php

namespace app\models\query;

use app\models\LessonAttachment;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\app\models\LessonAttachment]].
 *
 * @see \app\models\LessonAttachment
 */
class LessonAttachmentQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return LessonAttachment[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return LessonAttachment|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
