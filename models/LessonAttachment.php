<?php

namespace app\models;

use app\models\query\LessonAttachmentQuery;
use app\models\query\LessonQuery;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "lesson_attachment".
 *
 * @property int $id
 * @property int $lesson_id ссылка на занятие
 * @property string $url url вложения
 * @property string $type тип вложения
 *
 * @property Lesson $lesson
 */
class LessonAttachment extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lesson_attachment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lesson_id', 'url'], 'required'],
            [['lesson_id'], 'integer'],
            [['url'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 15],
            [['lesson_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lesson::className(), 'targetAttribute' => ['lesson_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lesson_id' => 'Lesson ID',
            'url' => 'Url',
            'type' => 'Type',
        ];
    }

    /**
     * @return ActiveQuery|LessonQuery
     */
    public function getLesson()
    {
        return $this->hasOne(Lesson::className(), ['id' => 'lesson_id']);
    }

    /**
     * {@inheritdoc}
     * @return LessonAttachmentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LessonAttachmentQuery(get_called_class());
    }
}
