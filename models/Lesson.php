<?php

namespace app\models;

use app\models\helpers\Inflector;
use app\models\query\ExerciseQuery;
use app\models\query\LessonAttachmentQuery;
use app\models\query\LessonContentQuery;
use app\models\query\LessonDependencyQuery;
use app\models\query\LessonQuery;
use app\models\query\LessonTypeQuery;
use app\models\query\SectionQuery;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "lesson".
 *
 * @property int $id
 * @property int $is_active
 * @property string $created_at
 * @property string $alias
 * @property string $label
 * @property string|null $description
 * @property int $section_id какому разделу принадлежит занятие
 * @property int $type_id тип занятия
 * @property int $difficulty_level сложность занятия
 *
 * @property Exercise[] $exercises
 * @property LessonType $type
 * @property Section $section
 * @property LessonContent[] $contents
 * @property LessonAttachment[] $attachments
 * @property LessonDependency[] $dependencies
 */
class Lesson extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lesson';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['is_active', 'section_id', 'type_id', 'difficulty_level'], 'integer'],
            [['created_at'], 'safe'],
            [['alias', 'label', 'section_id'], 'required'],
            [['description'], 'string'],
            [['alias', 'label'], 'string', 'max' => 255],
            [['alias'], 'unique'],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => LessonType::className(), 'targetAttribute' => ['type_id' => 'id']],
            [['section_id'], 'exist', 'skipOnError' => true, 'targetClass' => Section::className(), 'targetAttribute' => ['section_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'is_active' => 'Активно',
            'created_at' => 'Создано',
            'alias' => 'Алиас',
            'label' => 'Название',
            'description' => 'Краткое описание',
            'section_id' => 'Раздел',
            'type_id' => 'Тип',
            'difficulty_level' => 'Сложность'
        ];
    }

    /**
     * @return ActiveQuery|ExerciseQuery
     */
    public function getExercises()
    {
        return $this->hasMany(Exercise::className(), ['lesson_id' => 'id']);
    }

    /**
     * @return ActiveQuery|LessonTypeQuery
     */
    public function getType()
    {
        return $this->hasOne(LessonType::className(), ['id' => 'type_id']);
    }

    /**
     * @return ActiveQuery|SectionQuery
     */
    public function getSection()
    {
        return $this->hasOne(Section::className(), ['id' => 'section_id']);
    }

    /**
     * @return ActiveQuery|LessonContentQuery
     */
    public function getContents()
    {
        return $this->hasMany(LessonContent::className(), ['lesson_id' => 'id']);
    }

    /**
     * @return ActiveQuery|LessonAttachmentQuery
     */
    public function getAttachments()
    {
        return $this->hasMany(LessonAttachment::className(), ['lesson_id' => 'id']);
    }

    /**
     * @return ActiveQuery|LessonDependencyQuery
     * @throws InvalidConfigException
     */
    public function getDependencies()
    {
        return $this->hasMany(LessonDependency::className(), ['lesson_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return LessonQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LessonQuery(get_called_class());
    }

    /**
     * Set default values
     * @return bool
     */
    public function beforeValidate()
    {
        # если запись новая, создадим для нее алиас
        if ($this->isNewRecord) {
            $this->alias = Inflector::createAlias($this->label, self::tableName());
        }
        return parent::beforeValidate();
    }
}
