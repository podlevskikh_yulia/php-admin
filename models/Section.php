<?php

namespace app\models;

use app\models\helpers\Inflector;
use app\models\query\CourseQuery;
use app\models\query\LessonQuery;
use app\models\query\SectionQuery;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "section".
 *
 * @property int $id
 * @property int $is_active
 * @property string $alias
 * @property string $label
 * @property string|null $description
 * @property int $course_id id курса которому принадлежит раздел
 *
 * @property Lesson[] $lessons
 * @property Course $course
 */
class Section extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'section';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['is_active', 'course_id'], 'integer'],
            [['alias', 'label', 'course_id'], 'required'],
            [['description'], 'string'],
            [['alias', 'label'], 'string', 'max' => 255],
            [['alias'], 'unique'],
            [['course_id'], 'exist', 'skipOnError' => true, 'targetClass' => Course::className(), 'targetAttribute' => ['course_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'is_active' => 'Активен',
            'alias' => 'Алиас',
            'label' => 'Название',
            'description' => 'Описание',
            'course_id' => 'Курс',
        ];
    }

    /**
     * @return ActiveQuery|LessonQuery
     */
    public function getLessons()
    {
        return $this->hasMany(Lesson::className(), ['section_id' => 'id']);
    }

    /**
     * @return ActiveQuery|CourseQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }

    /**
     * {@inheritdoc}
     * @return SectionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SectionQuery(get_called_class());
    }

    /**
     * Set default values
     * @return bool
     */
    public function beforeValidate()
    {
        # если запись новая, создадим для нее алиас
        if ($this->isNewRecord) {
            $this->alias = Inflector::createAlias($this->label, self::tableName());
        }
        return parent::beforeValidate();
    }
}
