<?php

namespace app\models;

use app\models\query\ExerciseAnswerQuery;
use app\models\query\ExerciseQuery;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "exercise_answer".
 *
 * @property int $id
 * @property int $is_active
 * @property int $is_true верный ответ, на случай если у задания может быть несколько вариантов ответа
 * @property int $exercise_id
 * @property string $answer вариант ответа
 *
 * @property Exercise $exercise
 */
class ExerciseAnswer extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'exercise_answer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['is_active', 'is_true', 'exercise_id'], 'integer'],
            [['exercise_id', 'answer'], 'required'],
            [['answer'], 'string'],
            [['exercise_id'], 'exist', 'skipOnError' => true, 'targetClass' => Exercise::className(), 'targetAttribute' => ['exercise_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'is_active' => 'Активен',
            'is_true' => 'Верный',
            'exercise_id' => 'Упражнение',
            'answer' => 'Ответ',
        ];
    }

    /**
     * @return ActiveQuery|ExerciseQuery
     */
    public function getExercise()
    {
        return $this->hasOne(Exercise::className(), ['id' => 'exercise_id']);
    }

    /**
     * {@inheritdoc}
     * @return ExerciseAnswerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ExerciseAnswerQuery(get_called_class());
    }
}
