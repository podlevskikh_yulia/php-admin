<?php

namespace app\models;

use app\models\helpers\Inflector;
use app\modules\user\models\User;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "course".
 *
 * @property int $id
 * @property int $owner_id user_id пользователя, создавшего курс
 * @property int $is_active
 * @property string $alias
 * @property string $label
 * @property string|null $description
 *
 * @property Section[] $sections
 * @property User $owner
 */
class Course extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'course';
    }

    public function behaviors()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['owner_id', 'alias', 'label'], 'required'],
            [['owner_id', 'is_active'], 'integer'],
            [['description'], 'string'],
            [['alias', 'label'], 'string', 'max' => 255],
            [['alias'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'owner_id' => 'Создатель',
            'is_active' => 'Активен',
            'alias' => 'Алиас',
            'label' => 'Название',
            'description' => 'Описание',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getSections()
    {
        return $this->hasMany(Section::className(), ['course_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }

    /**
     * Set default values
     * @return bool
     */
    public function beforeValidate()
    {
        # если запись новая, создадим для нее алиас
        if ($this->isNewRecord) {
            $this->alias = Inflector::createAlias($this->label, self::tableName());
        }
        return parent::beforeValidate();
    }
}
