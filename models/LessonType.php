<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lesson_type".
 *
 * @property int $id
 * @property string $alias
 * @property string $label
 *
 * @property Lesson[] $lessons
 */
class LessonType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lesson_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['alias', 'label'], 'required'],
            [['alias', 'label'], 'string', 'max' => 255],
            [['alias'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alias' => 'Alias',
            'label' => 'Label',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery|\app\models\query\LessonQuery
     */
    public function getLessons()
    {
        return $this->hasMany(Lesson::className(), ['type_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\LessonTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\LessonTypeQuery(get_called_class());
    }
}
