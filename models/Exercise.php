<?php

namespace app\models;

use app\models\helpers\Inflector;
use app\models\query\ExerciseAnswerQuery;
use app\models\query\ExerciseQuery;
use app\models\query\ExerciseTypeQuery;
use app\models\query\LessonQuery;
use Throwable;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\StaleObjectException;

/**
 * This is the model class for table "exercise".
 *
 * @property int $id
 * @property int $is_active
 * @property int $is_final является ли упражнение частью итоговой работы
 * @property string $created_at
 * @property string $alias
 * @property string $label
 * @property string $description текст задания, описание (html)
 * @property int $type_id тип задания
 * @property int $lesson_id занятие
 * @property int $difficulty_level уровень сложности, 1 - просто, далее в сторону увеличения
 * @property int $sort сортировка
 *
 * @property ExerciseType $type
 * @property Lesson $lesson
 * @property ExerciseAnswer[] $answers
 */
class Exercise extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'exercise';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['is_active', 'is_final', 'type_id', 'lesson_id', 'difficulty_level', 'sort'], 'integer'],
            [['created_at'], 'safe'],
            [['alias', 'label', 'description', 'lesson_id'], 'required'],
            [['description'], 'string'],
            [['alias', 'label'], 'string', 'max' => 255],
            [['alias'], 'unique'],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ExerciseType::className(), 'targetAttribute' => ['type_id' => 'id']],
            [['lesson_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lesson::className(), 'targetAttribute' => ['lesson_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'is_active' => 'Активно',
            'is_final' => 'Итоговое',
            'created_at' => 'Создано',
            'alias' => 'Алиас',
            'label' => 'Название',
            'description' => 'Описание',
            'type_id' => 'Тип',
            'lesson_id' => 'Занятие',
            'difficulty_level' => 'Уровень сложности',
            'sort' => 'Сортировка',
        ];
    }

    /**
     * @return ActiveQuery|ExerciseTypeQuery
     */
    public function getType()
    {
        return $this->hasOne(ExerciseType::className(), ['id' => 'type_id']);
    }

    /**
     * @return ActiveQuery|LessonQuery
     */
    public function getLesson()
    {
        return $this->hasOne(Lesson::className(), ['id' => 'lesson_id']);
    }

    /**
     * @return ActiveQuery|ExerciseAnswerQuery
     */
    public function getAnswers()
    {
        return $this->hasMany(ExerciseAnswer::className(), ['exercise_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return ExerciseQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ExerciseQuery(get_called_class());
    }

    /**
     * Attach answers
     * @param $post array data from form
     * @return bool
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function attachAnswers($post)
    {
        foreach ($this->answers as $old_answer) {
            $old_answer->delete();
        }

        $answer = new ExerciseAnswer();
        $answer->load($post);
        $answer->exercise_id = $this->id;
        $answer->is_true = true;
        return $answer->save();
    }

    /**
     * Set default values
     * @return bool
     */
    public function beforeValidate()
    {
        # если запись новая, создадим для нее алиас
        if ($this->isNewRecord) {
            $this->alias = Inflector::createAlias($this->label, self::tableName());
        }
        return parent::beforeValidate();
    }
}
