<?php

namespace app\models;

use app\models\query\ExerciseQuery;
use app\models\query\ExerciseTypeQuery;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "exercise_type".
 *
 * @property int $id
 * @property string $alias
 * @property string $label
 *
 * @property Exercise[] $exercises
 */
class ExerciseType extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'exercise_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['alias', 'label'], 'required'],
            [['alias', 'label'], 'string', 'max' => 255],
            [['alias'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alias' => 'Алиас',
            'label' => 'Название',
        ];
    }

    /**
     * @return ActiveQuery|ExerciseQuery
     */
    public function getExercises()
    {
        return $this->hasMany(Exercise::className(), ['type_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return ExerciseTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ExerciseTypeQuery(get_called_class());
    }
}
