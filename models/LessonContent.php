<?php

namespace app\models;

use app\models\query\LessonContentQuery;
use app\models\query\LessonQuery;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "lesson_content".
 *
 * @property int $id
 * @property int $lesson_id
 * @property string $content
 * @property int $sort_weight
 * @property string $label
 *
 * @property Lesson $lesson
 */
class LessonContent extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lesson_content';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lesson_id', 'content'], 'required'],
            [['lesson_id', 'sort_weight'], 'integer'],
            [['content', 'label'], 'string'],
            [['lesson_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lesson::className(), 'targetAttribute' => ['lesson_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lesson_id' => 'ID занятия',
            'content' => 'Контент',
            'sort_weight' => 'Сортровка',
            'label' => 'Лейбл'
        ];
    }

    /**
     * @return ActiveQuery|LessonQuery
     */
    public function getLesson()
    {
        return $this->hasOne(Lesson::className(), ['id' => 'lesson_id']);
    }

    /**
     * {@inheritdoc}
     * @return LessonContentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LessonContentQuery(get_called_class());
    }
}
