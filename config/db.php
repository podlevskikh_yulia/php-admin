<?php
/**
 * PostgresQL
 */

$host = $port = $username = $password = $dbname = '';

$url = parse_url(env("DATABASE_URL"));
if (isset($url["host"]) && isset($url["user"]) && isset($url["path"])) {
    $host = $url["host"];
    $port = $url["port"];
    $username = $url["user"];
    $password = $url["pass"] ?? '';
    $dbname = substr($url["path"], 1);
}

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'pgsql:host=' . $host . ';port=' . $port . ';dbname=' . $dbname,
    'username' => $username,
    'password' => $password,
    'charset' => 'utf8',
];

/**
 * MySQL
 */
//return [
//    'class' => 'yii\db\Connection',
//    'dsn' => 'mysql:host=localhost;dbname=na100',
//    'username' => 'root',
//    'password' => '',
//    'charset' => 'utf8',
//
//    // Schema cache options (for production environment)
//    //'enableSchemaCache' => true,
//    //'schemaCacheDuration' => 60,
//    //'schemaCache' => 'cache',
//];
