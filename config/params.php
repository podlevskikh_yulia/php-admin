<?php

$pass = require __DIR__ . '/pass.php';

return [
    'adminEmail' => 'podlevskikh.yulia@gmail.com',
    'master_pass' => $pass,
    'bsVersion' => '4.x',
];
