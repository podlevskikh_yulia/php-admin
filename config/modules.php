<?php
/**
 * Created by PhpStorm.
 * User: Юля
 * Date: 02.04.2018
 * Time: 20:59
 */

return [
    'admin' => [
        'class' => 'app\modules\admin\Module',
    ],
    'user' => [
        'class' => 'app\modules\user\Module',
        'emailConfirmation' => true,
    ],
    'api' => [
        'class' => 'app\modules\api\Module'
    ],
];