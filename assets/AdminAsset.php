<?php
/**
 * Created by PhpStorm.
 * User: a.vladislav
 * Date: 03.09.2018
 * Time: 22:50
 */

namespace app\assets;


use yii\web\AssetBundle;

class AdminAsset extends AssetBundle
{

    public $css = [
        '/resources/admin/vendors/iconfonts/mdi/css/materialdesignicons.min.css',
        '/resources/admin/vendors/css/vendor.bundle.base.css',
        '/resources/admin/vendors/css/vendor.bundle.addons.css',
        '/resources/admin/vendors/summernote/dist/summernote-bs4.css',
        '/resources/admin/vendors/icheck/skins/all.css',
        '/resources/admin/css/style.css',
        '/resources/admin/css/overrides.css',
    ];

    public $js = [
        '/resources/admin/vendors/popper/popper.min.js',
        '/resources/admin/vendors/bootstrap/bootstrap.min.js',
        '/resources/admin/vendors/chartjs/Chart.min.js',
        '/resources/admin/vendors/summernote/dist/summernote-bs4.min.js',
        '/resources/admin/vendors/summernote/dist/lang/summernote-ru-RU.min.js',
        '/resources/admin/js/off-canvas.js',
        '/resources/admin/js/misc.js',
        '/resources/admin/js/dashboard.js',
        '/resources/admin/js/init-components.js',
    ];

    public $depends = [
        'yii\web\YiiAsset'
    ];

}