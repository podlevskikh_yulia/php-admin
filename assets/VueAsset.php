<?php
/**
 * Created by PhpStorm.
 * User: a.vladislav
 * Date: 03.09.2018
 * Time: 22:50
 */

namespace app\assets;


use yii\web\AssetBundle;
use yii\web\View;

class VueAsset extends AssetBundle
{

    public function init()
    {

        $this->jsOptions['position'] = View::POS_BEGIN;

        parent::init();

    }

    public $js = [
        '/resources/common/vendors/vuejs/vue.min.js',
    ];

}