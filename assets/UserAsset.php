<?php


namespace app\assets;


use yii\web\AssetBundle;

class UserAsset extends AssetBundle
{

    public $css = [
        '/resources/admin/vendors/bootstrap/bootstrap.min.css',
        '/resources/user/css/style.css',
    ];

}